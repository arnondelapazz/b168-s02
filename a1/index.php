<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity S02</title>
</head>
<body>

	<h1>Divisible by 5</h1>

	<?php divisibleByFive() ?>

	<h1>Array Manipulation</h1>

	<?php array_push($studentNames, 'Mark'); ?>

	<?php print_r($studentNames); ?>

	<pre><?php echo count($studentNames); ?></pre>

	<?php array_push($studentNames, 'John'); ?>

	<?php print_r($studentNames); ?>

	<pre><?php echo count($studentNames); ?></pre>

	<?php array_pop($studentNames); ?>

	<?php print_r($studentNames); ?>

	<pre><?php echo count($studentNames); ?></pre>



</body>
</html>